//DOM Manipulation through hacky jQuery

guid = "";
password = "";
address = "";
btc = 0.0;
usd = 0.0;
confirmation = false;

//$('.send-by li').click(function () {
$(document).on("click", ".send-by li", function () {
    $('.send-by li').removeClass('active');
    if ($(this).hasClass('by-email')) {
        $('.to-field div').addClass('hide');
        $('.email').removeClass('hide');
    } else if ($(this).hasClass('by-sms')) {
        $('.to-field div').addClass('hide');
        $('.phone').removeClass('hide');
    } else {
        $('.to-field div').addClass('hide');
        $('.btc').removeClass('hide');
    }
    $(this).addClass('active');
});

//$('.next').click(function() {
$(document).on("click", ".next", function () {
    if (confirmation == false) {
        login();
    } else {
        makePayment();
    }
})

//$('.cancel').click(function () {
$(document).on("click", ".cancel", function () {
    confirmation = false;
    
    $('.form-wrapper').addClass('collapse');

    $('.step-2').addClass('hide');
    $('.step-1').removeClass('hide');

    $('.step').html('Step 1 of 2');
    $(this).html('Cancel');

    $('.next').removeClass('send');
    $('.next').text('Next');
    $('.next').find('i').removeClass('hidden');
    setTimeout(function () {
        $('.form-wrapper').removeClass('collapse');
    }, 1000);
})

//$('#btc-amount').change(function () {
$(document).on("input", "#btc-amount", function () {
    $btcValue = $(this).val();
    $('#usd-amount').val($btcValue * 7206.79);
})

//$('#usd-amount').change(function () {
$(document).on("input", "#usd-amount", function () {
    $usdValue = $(this).val();
    $('#btc-amount').val($usdValue * .00014);
})

function login() {
    guid = $("#btcFrom").val();
    password = $("#btcPass").val();
    fetch('http://localhost:8018/fetching-the-wallet-balance?guid=' + guid + '&password=' + password, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            if (data.balance != null) {
                sweetAlert("success", "Login successful");
                address = $('#btcTo').val();
                btc = $('#btc-amount').val();
                usd = $('#usd-amount').val();
                confirmation = true;

                $('.form-wrapper').addClass('collapse');

                $('.step-1').addClass('hide');
                $('.step-2').removeClass('hide');

                $('.step').html('Step 2 of 2');
                $('.cancel').html('Back');

                $('.next').addClass('send');
                $('.next').text('Send');
                $('.next').find('i').addClass('hidden');
                setTimeout(function () {
                    $('.form-wrapper').removeClass('collapse');
                }, 1000);

                $('#address-final').val(address);
                $('#btc-amount-total').text(btc + " BTC");
                $('#usd-amount-total').text("$" + usd + " USD");
                btc = parseFloat(btc) + 0.0001;
                usd = parseFloat(usd) + 0.72;
                $('#btc-amount-final').text(btc + " BTC");
                $('#usd-amount-final').text("$" + usd + " USD");
            } else {
                sweetAlert("error", "Login unsuccessful due to incorrect credentials");
            }
        })
        .catch(err => {
            console.log("There was an error in login() method");
        })
}

function makePayment() {
    var satoshi = parseFloat(btc) * 100000000;
    fetch('http://localhost:8018/make-payment?guid=' + guid + '&password=' + password + '&to=' + address + '&amount=' + satoshi, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            if (data.error == null) {
                sweetAlert("success", "Payment successful");
            } else {
                sweetAlert2("Payment unsuccessful", "Insufficient funds");
            }
        })
        .catch(err => {
            console.log("There was an error in makePayment() method");
        })
}

function sweetAlert(type, title) {
    Swal.fire({
        position: 'top-end',
        icon: type,
        title: title,
        showConfirmButton: false,
        timer: 1500
    })
}

function sweetAlert2(title, text) {
    Swal.fire({
        icon: 'error',
        title: title,
        text: text
    })
}